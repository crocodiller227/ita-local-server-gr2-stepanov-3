<?php

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{
    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('index_page'));
    }

    /**
     * @When /^я нажимаю на ссылку "([^"]*)"$/
     */
    public function яНажимаюНаСсылку($arg1)
    {
        $this->getSession()->getPage()->findLink($arg1)->click();
    }

    /**
     * @When /^я вижу форму$/
     */
    public function ЯВижуФорму()
    {
        $this->getSession()->getPage()->find('css', 'form');
    }

    /**
     * @When /^я заполняю форму регистрации, введя в поле "([^"]*)" значение "([^"]*)", в поле "([^"]*)" "([^"]*)", в поле"([^"]*)" "([^"]*)", в поле "([^"]*)" "([^"]*)" и выбираю роль в селекторе "([^"]*)" "([^"]*)"$/
     */
    public function яЗаполняюФормуРегистрацииВведяВПолеЗначениеВПолеВПолеВПолеИВыбираюРольВСелекторе($arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10)
    {
        $this->fillField($arg1, $arg2);
        $this->fillField($arg3, $arg4);
        $this->fillField($arg5, $arg6);
        $this->fillField($arg7, $arg8);
        $this->fillField($arg9, $arg10);
        $this->pressButton('user_register_register');
    }

    /**
     * @When /^я перехожу на страницу "([^"]*)"$/
     */
    public function яПерехожуНаСтраницуАвторизации($arg1)
    {
        $this->visit($arg1);
    }

    /**
     * @When /^я заполняю форму авторизации, заполнив поле "([^"]*)" значением "([^"]*)" и поле "([^"]*)" значением "([^"]*)"$/
     */
    public function яЗаполняюФормуАвторизацииЗаполнивПолеЗначениемИПолеЗначением($arg1, $arg2, $arg3, $arg4)
    {
        $this->fillField($arg1, $arg2);
        $this->fillField($arg3, $arg4);
        $this->pressButton('user_authentication_sign in');
    }


    /**
     * @When /^я заполняю форму добавления объекта бронирования, вводя в поле "([^"]*)" значение "([^"]*)", в поле "([^"]*)" значение "([^"]*)", в поле "([^"]*)" значение "([^"]*)", в поле "([^"]*)" значение "([^"]*)"в поле r"([^"]*)" значение"([^"]*)"$/
     */
    public function яЗаполняюФормуДобавленияОбъектаБронированияВводяВПолеЗначениеВПолеЗначениеВПолеЗначениеВПолеЗначениеВПолеЗначение($arg1, $arg2, $arg3, $arg4, $arg5, $arg6, $arg7, $arg8, $arg9, $arg10)
    {
        $this->fillField($arg1, $arg2);
        $this->fillField($arg3, $arg4);
        $this->fillField($arg5, $arg6);
        $this->fillField($arg7, $arg8);
        $this->getSession()->getPage()->findById('map')->click();
        $this->fillField($arg9, $arg10);
        $this->pressButton('register_booking_object_next_step_>');
        sleep(100);
    }

    /**
     * @When /^я начинаю нажимать на кнопки бронирования, пока не увижу сообщение "([^"]*)" на странице$/
     */
    public function яНачинаюНажиматьНаКнопкиБронированияПокаНеУвижуСообщениеНаСтранице($arg1)
    {
        $this->clickLink('Booking');
        $this->clickLink('Booking');
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я заполняю форму опциями "([^"]*)"$/
     */
    public function яЗаполняюФормуОпциями($arg1)
    {
        $this->checkOption($arg1);
        $this->pressButton('register_cottage_options_register');
    }


}

