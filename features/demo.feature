# language: ru

Функционал: Тестируем скудный функционал нашего букинга. ВНИМАНИЕ! ДЛЯ ДОСТУПА К АПИ, ТРЕБУЕТСЯ ЗАГРУЗИТЬ ФИКСТУРЫ!

  Сценарий: Регистрируемся арендодателем на нашем сайте
    Допустим я нахожусь на главной странице
    И я вижу слово "Register" где-то на странице
    Тогда я нажимаю на ссылку "Register"
    И я вижу форму
    Тогда я заполняю форму регистрации, введя в поле "user_register_username" значение "landlord_666", в поле "user_register_email" "landlord666@bookong.root", в поле"user_register_passport_id" "LL666", в поле "user_register_password" "root" и выбираю роль в селекторе "user_register_roles" "landlord"
    И я вижу слово "Login" где-то на странице

  Сценарий: Авторизуемся, как арендодатель и регистрирую свой объект бронирования
    Допустим я перехожу на страницу "/user/login"
    Тогда я заполняю форму авторизации, заполнив поле "user_authentication_email" значением "landlord666@bookong.root" и поле "user_authentication_password" значением "root"
    И я вижу слово "Landlord objects" где-то на странице
    И я нажимаю на ссылку "Register new object"
    И я вижу форму
    Тогда я заполняю форму добавления объекта бронирования, вводя в поле "register_booking_object_object_name" значение "Cottage1", в поле "register_booking_object_number_of_rooms" значение "5", в поле "register_booking_object_contact_person" значение "DyadyaVasya", в поле "register_booking_object_contact_person_phone" значение "0550500500"в поле r"register_booking_object_price_per_night" значение"300"
    И я вижу слово "Cottage1" где-то на странице

  Сценарий: Регистрируемся, как арендатор, авторизуемся и ронируем объект
    Допустим  я перехожу на страницу "/user/register"
    И я вижу форму
    Тогда я заполняю форму опциями "register_cottage_options_kitten"
    Тогда я заполняю форму регистрации, введя в поле "user_register_username" значение "tenant_777", в поле "user_register_email" "tenant777@bookong.root", в поле"user_register_passport_id" "TT666", в поле "user_register_password" "root" и выбираю роль в селекторе "user_register_roles" "tenant"
    И я вижу слово "Login" где-то на странице
    И я заполняю форму авторизации, заполнив поле "user_authentication_email" значением "tenant777@bookong.root" и поле "user_authentication_password" значением "root"
    И я вижу слово "Cottage1" где-то на странице
    Тогда я начинаю нажимать на кнопки бронирования, пока не увижу сообщение "You have already booked the property. Wait until the end of the reservation." на странице