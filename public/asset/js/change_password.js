$(function () {
    $('form[name="user_change_password"]').submit(function (event) {
        event.preventDefault();
        let form = $(this);
        let message_div = form.closest('.card_user_setting').children('.message');
        let form_data = {};
        $.each(form.serializeArray(), function (i, field) {
            form_data[field.name] = field.value;
        });
        $.ajax({
            type: "POST",
            url: $('body').data('root-url') + 'user/change_password',
            data: form_data,
            success: function (response) {
                message_div.addClass('alert-success');
                message_div.children('p').text(response.message);
                message_div.fadeIn(1000);
                message_div.fadeOut(5000);
                setTimeout(function () {
                    message_div.removeClass('alert-success');
                }, 3000);
                console.log(response);
            },
            error: function (response) {
                message_div.addClass('alert-danger');
                message_div.children('p').text(response.responseJSON.message || 'Error');
                message_div.fadeIn(1000);
                message_div.fadeOut(5000);
                console.log(response);
                setTimeout(function () {
                    message_div.removeClass('alert-danger');
                }, 3000);
            }
        });
    });
});