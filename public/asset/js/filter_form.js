$(document).ready(function () {
    $('.filter_object_type').on('change', function () {
        let radio = $(this);
        let cottage = $('.cottage_checkbox');
        let pension = $('.pension_checkbox');
        if (radio.attr('id') === 'booking_object_pension') {
            cottage.hide();
            pension.show();
        } else if (radio.attr('id') === 'booking_object_cottage') {
            cottage.show();
            pension.hide();
        }
    });
});