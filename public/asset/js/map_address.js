function initMap() {
    let form_address = $('#register_booking_object_address');

    form_address.after('<div id="map" style="height: 400px"></div>');

    let uluru = {lat: 42.646514, lng: 77.078232};
    let map = new google.maps.Map(
        document.getElementById('map'), {zoom: 8, center: uluru});
    let marker;

    google.maps.event.addListener(map, 'click', function (event) {
        if (marker) {
            marker.setPosition(event.latLng);
        } else {
            marker = new google.maps.Marker({
                position: event.latLng,
                map: map
            });
        }

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
        form_address.val('http://www.google.com/maps/place/' + marker.getPosition().lat() + ',' + marker.getPosition().lng());
    });
}