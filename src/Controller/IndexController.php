<?php

namespace App\Controller;


use App\Entity\Booking;
use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Application\ErrorHandler;
use App\Model\BookingObjects\BookingObjectsHandler;
use App\Model\Enum\UserEnum;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index_page")
     * @param ApiContext $apiContext
     * @param BookingObjectsHandler $bookingObjectsHandler
     * @param ErrorHandler $errorHandler
     * @return RedirectResponse|Response
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function pingAction(
        ApiContext $apiContext,
        BookingObjectsHandler $bookingObjectsHandler,
        ErrorHandler $errorHandler
    )
    {
        $error = null;


        /** @var User $currentUser */
        $currentUser = $this->getUser();
        if ($currentUser) {
            if (in_array(UserEnum::ROLE_LANDLORD, $currentUser->getRoles())) {
                return $this->redirectToRoute('show_all_my_object');
            }
        }
        try {
            $objects = $apiContext->getAllBookingObjects();
            if ($objects) {
                $bookingObjects = $bookingObjectsHandler->arrayIntoArrayEntities($objects['booking_objects']);
            }
        } catch (ApiException $e) {
        }

        $error = $errorHandler->getErrorMessageOfSession($error);

        return $this->render('index.html.twig', [
            'objects' => $bookingObjects ?? null,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/objects/filter", name="filter_booking_object")
     * @Method("POST")
     * @param Request $request
     * @param ApiContext $apiContext
     * @param BookingObjectsHandler $bookingObjectsHandler
     * @param ErrorHandler $errorHandler
     * @return Response
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function filterBookingObjectsActions(
        Request $request,
        ApiContext $apiContext,
        BookingObjectsHandler $bookingObjectsHandler,
        ErrorHandler $errorHandler
    )
    {
        $form_data = $request->request->all();
        foreach ($form_data as $key => $field) {
            if ($field == '') {
                unset($form_data[$key]);
            }
        }
        $error = null;
        try {
            $objects = $apiContext->getBookingObjectsWithFilter($form_data);
        } catch (ApiException $e) {
            $error = $e->getMessage();
        }
        try {
            $bookingObjects = $bookingObjectsHandler->arrayIntoArrayEntities($objects['booking_objects']);
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        $error = $errorHandler->getErrorMessageOfSession($error);

        return $this->render('index.html.twig', [
            'objects' => $bookingObjects ?? null,
            'error' => $error,
        ]);
    }

    /**
     * @Route("/object/booking/create/{object_name}/{room_number}", name="create_booking")
     * @param $object_name
     * @param $room_number
     * @param ApiContext $apiContext
     * @param ErrorHandler $errorHandler
     * @return RedirectResponse
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function createBooking(
        $object_name,
        $room_number,
        ApiContext $apiContext,
        ErrorHandler $errorHandler
    )
    {
        $booking = new Booking();
        $booking
            ->setBookingObjectName($object_name)
            ->setRoomNumber($room_number)
            ->setTenantEmail($this->getUser()->getEmail());
        try {
            $apiContext->createBooking($booking->__toArray());
        } catch (ApiException $e) {
            $errorHandler->setErrorIntoSession($e->getResponse()['error'] ?? $e->getMessage());
        }
        return $this->redirectToRoute('index_page');
    }
}
