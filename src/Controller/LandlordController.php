<?php


namespace App\Controller;


use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Landlord;
use App\Entity\Pension;
use App\Form\RegisterBookingObjectType;
use App\Form\RegisterCottageOptionsType;
use App\Form\RegisterPensionOptionsType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Application\ErrorHandler;
use App\Model\BookingObjects\BookingObjectsHandler;
use App\Model\Enum\BookingObjectEnum;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LandlordController
 * @package App\Controller
 * @Route("/landlord")
 */
class LandlordController extends Controller
{

    /**
     * @Route("/object/register", name="register_object")
     * @param Request $request
     * @param ErrorHandler $errorHandler
     * @return RedirectResponse|Response
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function registrationBookingObjectAction(
        Request $request,
        ErrorHandler $errorHandler
    )
    {
        $error = '';

        $bookingObject = new BookingObject();
        $registerObjectForm = $this->createForm(RegisterBookingObjectType::class, $bookingObject);


        $registerObjectForm->handleRequest($request);
        if ($registerObjectForm->isSubmitted() && $registerObjectForm->isValid()) {
            $this->get('session')->set('booking_object', $bookingObject);
            return $this->redirectToRoute('set_booking_object_options');
        }

        $error = $errorHandler->getErrorMessageOfSession($error);

        return $this->render('objects\register.html.twig', [
            'register_object_form' => $registerObjectForm->createView(),
            'error' => $error,
        ]);
    }

    /**
     * @Route("/object/register/options", name="set_booking_object_options")
     * @param Request $request
     * @param ApiContext $apiContext
     * @param ErrorHandler $errorHandler
     * @return RedirectResponse|Response
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function selectOptionFromRegisterBookingObjectAction(
        Request $request,
        ApiContext $apiContext,
        ErrorHandler $errorHandler
    )
    {

        /** @var BookingObject $booking_object */
        $booking_object = $this->get('session')->get('booking_object');
        $booking_object->setLandlordEmail($this->getUser()->getEmail());

        if ($booking_object->getObjectType() == BookingObjectEnum::BOOKING_OBJECT_COTTAGE) {
            $cottage = new Cottage();
            $booking_object = $cottage->__fromArray($booking_object->__toArray());
            $form = $this->createForm(RegisterCottageOptionsType::class, $booking_object);
        } elseif ($booking_object->getObjectType() == BookingObjectEnum::BOOKING_OBJECT_PENSION) {
            $pension = new Pension();
            $booking_object = $pension->__fromArray($booking_object->__toArray());
            $form = $this->createForm(RegisterPensionOptionsType::class, $booking_object);
        } else {
            return $this->redirectToRoute('register_object');
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('session')->remove('booking_object');
            try {
                $apiContext->createNewObject($booking_object->__toArray());
            } catch (ApiException $e) {
                $errorHandler
                    ->setErrorIntoSession(
                        'Oops, an error occurred during the creation of the new object. Try again'
                    );
                return $this->redirectToRoute('register_object');
            }
            return $this->redirectToRoute('index_page');
        }

        return $this->render('objects/register_options.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/object/show/my", name="show_all_my_object")
     * @param ApiContext $apiContext
     * @param BookingObjectsHandler $bookingObjectsHandler
     * @param ErrorHandler $errorHandler
     * @return Response
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function showMyBookingObjectsAction(
        ApiContext $apiContext,
        BookingObjectsHandler $bookingObjectsHandler,
        ErrorHandler $errorHandler
    )
    {
        $error = '';
        /** @var Landlord $currentLandlord */
        $currentLandlord = $this->getUser();
        try {
            $objects = $apiContext->getLandlordObjects($currentLandlord->getEmail());
        } catch (ApiException $e) {
            $error = $e->getMessage();
        }
        $error = $errorHandler->getErrorMessageOfSession($error);
        $bookingObjects = $bookingObjectsHandler->arrayIntoArrayEntities($objects['booking_objects']);
        return $this->render('objects/all_landlord_objects.html.twig', [
            'objects' => $bookingObjects,
            'error' => $error
        ]);
    }
}