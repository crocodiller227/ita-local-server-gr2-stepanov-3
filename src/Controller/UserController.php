<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\UserAuthenticationType;
use App\Form\UserChangePasswordType;
use App\Form\UserEditType;
use App\Form\UserRegisterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Enum\SocialEnum;
use App\Model\Enum\UserEnum;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function userRegisterAction(
        Request $request,
        UserHandler $userHandler,
        ApiContext $apiContext,
        EntityManagerInterface $entityManager
    )
    {
        $user = new User;
        $error = '';
        $form = $this->createForm(UserRegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassportId(), $user->getEmail())) {
                    $error = 'Error: A user with such data is already registered. You need to log in.';
                } else {
                    $user->setEnabled(true);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    if (in_array(UserEnum::ROLE_TENANT, $data[UserEnum::ROLES])) {
                        $user = $userHandler->createNewTenant($data);
                    } elseif (in_array(UserEnum::ROLE_LANDLORD, $data[UserEnum::ROLES])) {
                        $user = $userHandler->createNewLandlord($data);
                    }
                    $entityManager->persist($user);
                    $entityManager->flush();

                    return $this->redirectToRoute('login');
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage();

            }
        }

        return $this->render('user/register.html.twig', [
            'register_form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/login", name="login")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param ApiContext $apiContext
     * @param UserHandler $userHandler
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws ApiException
     */
    public function loginAction(
        UserRepository $userRepository,
        Request $request,
        ApiContext $apiContext,
        UserHandler $userHandler,
        EntityManagerInterface $entityManager
    )
    {
        $error = null;
        $form = $this->createForm(UserAuthenticationType::class);

        $form->handleRequest($request);
        $user = null;


        if (isset($_POST['token'])) {
            $socialData = json_decode(
                file_get_contents('http://ulogin.ru/token.php?token='
                    . $_POST['token'] . '&host='
                    . $_SERVER['HTTP_HOST']),
                true);

            $user = $userRepository->findUserBySocial($socialData);
            if (!$user) {
                if ($apiContext->checkClientBySocial($socialData)) {
                    try {
                        $user_data = $apiContext->getClient([
                            SocialEnum::NETWORK => $socialData[SocialEnum::NETWORK],
                            SocialEnum::UID => $socialData[SocialEnum::UID]]);
                        $user = $userRepository->findUserByEmail($user_data[UserEnum::EMAIL]);
                        if ($user) {
                            $user->__fromArray($user_data);
                            $entityManager->flush();
                        } else {
                            $user = new User();
                            $user->__fromArray($user_data);
                            $entityManager->persist($user);
                            $entityManager->flush();
                        }
                    } catch (\Exception $e) {
                        $error = 'Error: ' . $e->getMessage();
                    }
                }
            }
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $password = $userHandler->passwordHashing($data[UserEnum::PASSWORD]);
            $user = $userRepository->findUserByEmailAndPassword($data[UserEnum::EMAIL], $password);

            if ($user) {
                $userHandler->insertUserInSession($user);
                return $this->redirectToRoute('index_page');
            }

            try {
                if ($apiContext->clientCheck($data[UserEnum::EMAIL], $password)) {
                    $client_data = $apiContext->getClient([UserEnum::EMAIL => $data[UserEnum::EMAIL]]);
                    if (in_array(UserEnum::ROLE_TENANT, $client_data[UserEnum::ROLES])) {
                        $user = $userHandler->createNewTenant($client_data);
                    } elseif (in_array(UserEnum::ROLE_LANDLORD, $client_data[UserEnum::ROLES])) {
                        $user = $userHandler->createNewLandlord($client_data);
                    }

                    $user
                        ->setPassportId($client_data['password'])
                        ->setEnabled(true);
                    $entityManager->persist($user);
                    $entityManager->flush();

                } else {
                    $error = 'Incorrect data for authorization has been entered. Try entering again or registering';
                }
            } catch (ApiException $e) {
                $error = 'An error occurred during authorization. Try it again';
            }
        }

        if ($user) {
            $userHandler->insertUserInSession($user);
            return $this->redirectToRoute('index_page');
        }

        return $this->render('user/login.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/profile/settings/", name="user_profile_settings")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function userProfileSettings(
        Request $request,
        EntityManagerInterface $em,
        ApiContext $apiContext
    )
    {
        $userEditForm = $this->createForm(UserEditType::class, $this->getUser());
        $changePasswordForm = $this->createForm(UserChangePasswordType::class, null,
            ['action' => $this->generateUrl('user_change_password')]);

        $current_email = $this->getUser()->getEmail();

        $userEditForm->handleRequest($request);
        if ($userEditForm->isSubmitted() && $userEditForm->isValid()) {
            if ($apiContext->editUser(
                array_merge($userEditForm->getData()->__toArray(), ['current_email' => $current_email]))
            ) {
                $em->flush();
            }
        }
        return $this->render('user/user_profile_settings.html.twig', [
            'user_edit' => $userEditForm->createView(),
            'change_password' => $changePasswordForm->createView()
        ]);
    }

    /**
     * @Route("/change_password", name="user_change_password")
     * @param Request $request
     * @param UserHandler $userHandler
     * @param EntityManagerInterface $em
     * @param ApiContext $apiContext
     * @return JsonResponse
     * @throws ApiException
     */
    public function changePasswordAction(
        Request $request,
        UserHandler $userHandler,
        EntityManagerInterface $em,
        ApiContext $apiContext
    )
    {
        $form_data = $request->request->all();
        $form_data = $form_data['user_change_password'];
        $response_message = 'Password successfully changed!';
        $response_status = 200;
        $currentUser = $this->getUser();
        if ($userHandler->passwordHashing($form_data['old_password']) == $currentUser->getPassword()) {
            $hashingNewPassword = $userHandler->passwordHashing($form_data['new_password']);
            if ($form_data['new_password'] === $form_data['new_password_retry']) {
                try {
                    $apiContext->changePassword([
                        'email' => $currentUser->getEmail(),
                        'password' => $hashingNewPassword]);
                } catch (ApiException $e) {
                    return new JsonResponse(['message' => 'Error: ' . $e->getMessage()], 406);
                }
                $currentUser->setPassword($hashingNewPassword);
                $em->flush();
            } else {
                $response_status = 412;
                $response_message = 'The new password you entered does not match the confirmation';
            }
        } else {
            $response_status = 401;
            $response_message = 'Old password incorrect';
        }
        return new JsonResponse(['message' => $response_message], $response_status);
    }

    /**
     * @Route("/social/binding", name="social_binding")
     * @param ApiContext $apiContext
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function socialBindingAction(
        ApiContext $apiContext,
        EntityManagerInterface $entityManager)
    {
        $socialData = json_decode(
            file_get_contents('http://ulogin.ru/token.php?token='
                . $_POST['token'] . '&host='
                . $_SERVER['HTTP_HOST']),
            true);

        /** @var User $currentUser */
        $currentUser = $this->getUser();
        try {
            $apiContext->bindSocialNetwork(array_merge($socialData, ['email' => $currentUser->getEmail()]));
        } catch (ApiException $e) {
            return $this->redirectToRoute('user_profile_settings');
        }
        $currentUser->bindSocialNetwork($socialData);
        $entityManager->flush();
        return $this->redirectToRoute('user_profile_settings');
    }

    /**
     * @Route("/register-case", name="app-register-case")
     */
    public function socialRegisterCaseAction()
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = new User();
        $user->setEmail($userData[UserEnum::EMAIL] ?? null);

        $form = $this->createForm(
            'App\Form\SocialRegisterType', $user, [
                'action' => $this
                    ->get('router')
                    ->generate('app-register-case2')
            ]
        );

        $this->get('session')->set('ulogin-data', $s);

        return $this->render('user/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function socialRegisterCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get('ulogin-data'),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\SocialRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassportId(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setUsername($userData[UserEnum::EMAIL])
                        ->setFullName($userData[SocialEnum::FIRST_NAME] . ' ' . $userData[SocialEnum::LAST_NAME])
                        ->bindSocialNetwork($userData);
                    $data = $user->__toArray();
                    $apiContext->createClient($data);
                    if (in_array(UserEnum::ROLE_LANDLORD, $user->getRoles())) {
                        $user = $userHandler->createNewLandlord($data);
                    } elseif (in_array(UserEnum::ROLE_TENANT, $user->getRoles())) {
                        $user = $userHandler->createNewTenant($data);
                    }

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("index_page");
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . var_export($e->getMessage(), 1);
            }
        }

        return $this->render('user/ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

}