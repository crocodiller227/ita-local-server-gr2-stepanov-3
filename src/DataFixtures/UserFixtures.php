<?php


namespace App\DataFixtures;


use App\Model\Enum\UserEnum;
use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \App\Model\Api\ApiException
     */
    public function load(ObjectManager $manager)
    {
        $hashingPassword = function ($plainPassword) {
            return password_hash($plainPassword,
                PASSWORD_BCRYPT,
                ['cost' => 12, 'salt' => md5('lolkekcheburek')]);
        };

        $landlord = $this->userHandler->createNewLandlord([
            UserEnum::EMAIL => 'landlord@bookong.root',
            UserEnum::PASSPORT_ID => 'LANDLORD666',
            UserEnum::USERNAME => 'landlord',
            UserEnum::ENABLED => true,
            UserEnum::PASSWORD => $hashingPassword('root'),
            UserEnum::FULL_NAME => 'Mr. Landlord',
        ], false);
        $manager->persist($landlord);

        $tenant = $this->userHandler->createNewTenant([
            UserEnum::EMAIL => 'tenant@bookong.root',
            UserEnum::PASSPORT_ID => 'TENANT666',
            UserEnum::USERNAME => 'tenant',
            UserEnum::ENABLED => true,
            UserEnum::PASSWORD => $hashingPassword('root'),
            UserEnum::FULL_NAME => 'Mr. Tenant',
        ], false);
        $manager->persist($tenant);

        $manager->flush();
    }
}