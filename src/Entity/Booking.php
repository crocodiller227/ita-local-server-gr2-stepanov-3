<?php

namespace App\Entity;

use App\Model\Enum\BookingObjectEnum;

class Booking
{

    /**
     * @var string
     */
    private $tenantEmail;

    /**
     * @var string
     */
    private $bookingObjectName;

    /**
     * @var int
     */
    private $roomNumber;

    public function __toArray()
    {
        return [
            'tenant_email' => $this->tenantEmail,
            BookingObjectEnum::BOOKING_OBJECT_NAME => $this->bookingObjectName,
            BookingObjectEnum::BOOKING_OBJECT_ROOM => $this->roomNumber
        ];
    }


    /**
     * @param \DateTime $dateFrom
     * @return Booking
     */
    public function setDateFrom(\DateTime $dateFrom): Booking
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom(): \DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @param string $tenantEmail
     * @return Booking
     */
    public function setTenantEmail(string $tenantEmail): Booking
    {
        $this->tenantEmail = $tenantEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getTenantEmail()
    {
        return $this->tenantEmail;
    }

    /**
     * @param mixed $bookingObjectName
     * @return Booking
     */
    public function setBookingObjectName($bookingObjectName)
    {
        $this->bookingObjectName = $bookingObjectName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBookingObjectName()
    {
        return $this->bookingObjectName;
    }

    /**
     * @param int $roomNumber
     * @return Booking
     */
    public function setRoomNumber(int $roomNumber): Booking
    {
        $this->roomNumber = $roomNumber;
        return $this;
    }

    /**
     * @return int
     */
    public function getRoomNumber(): int
    {
        return $this->roomNumber;
    }


}
