<?php

namespace App\Entity;

use App\Model\Enum\BookingObjectEnum;
use App\Model\Enum\UserEnum;
use Symfony\Component\Validator\Constraints as Assert;

class BookingObject
{

    /**
     * @var string
     * @Assert\NotBlank(message="Object name can not be empty")
     */
    protected $objectName;

    /**
     * @var string
     * @Assert\NotBlank(message="Address can not be empty")
     * @Assert\Url()
     */
    protected $address;

    /**
     * @var string
     * @Assert\NotBlank(message="Contact person field can not be empty")
     * @Assert\Type(
     *     type="string",
     *     message="Incorrect data in the Contact person field. The data is false to have type {{ type }}"
     * )
     */
    protected $contactPerson;

    /**
     * @var string
     * @Assert\NotBlank(message="The telephone number of the contact person can not be empty")
     * @Assert\Regex(
     *     pattern="/^((0|\+996)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{6}$/",
     *     message="Phone number of the contact person is entered incorrectly"
     * )
     */
    protected $contactPersonPhone;

    /**
     * @var int
     * @Assert\NotBlank(message="Number of rooms can not be empty")
     * @Assert\Type(
     *     type="int",
     *     message="The number of rooms is incorrect. Data should be {{ type }}"
     * )
     */
    protected $numberOfRooms;

    /**
     * @var float
     * @Assert\NotBlank(message="Price field per night can not remain empty")
     * @Assert\Type(
     *     type="float",
     *     message="The price is not indicated correctly"
     * )
     */
    protected $pricePerNight;

    /**
     * @var string
     * @Assert\NotBlank(message="Object type field can not remain empty")
     */
    protected $object_type;

    /**
     * @var string
     */
    protected $landlordEmail;

    protected $room;

    protected $last_booking;

    /**
     * @return array
     */
    public function __toArray()
    {
        return [
            BookingObjectEnum::BOOKING_OBJECT_ADDRESS => $this->address,
            BookingObjectEnum::BOOKING_OBJECT_NAME => $this->objectName,
            BookingObjectEnum::BOOKING_OBJECT_TYPE => $this->object_type,
            BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON => $this->contactPerson,
            BookingObjectEnum::BOOKING_OBJECT_NUMBER_OF_ROOMS => $this->numberOfRooms,
            BookingObjectEnum::BOOKING_OBJECT_PRICE_PER_NIGHT => $this->pricePerNight,
            BookingObjectEnum::BOOKING_OBJECT_LANDLORD => $this->landlordEmail,
            BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON_PHONE => $this->contactPersonPhone,
            BookingObjectEnum::BOOKING_OBJECT_ROOM => $this->room,
            BookingObjectEnum::BOOKING_OBJECT_LAST_BOOKING => $this->last_booking,
        ];
    }

    /**
     * @param array $bookingObjectData
     * @return $this
     */
    public function __fromArray(array $bookingObjectData)
    {
        $this->address = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_ADDRESS];
        $this->objectName = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_NAME];
        $this->object_type = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_TYPE];
        $this->contactPerson = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON];
        $this->numberOfRooms = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_NUMBER_OF_ROOMS];
        $this->pricePerNight = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_PRICE_PER_NIGHT];
        $this->landlordEmail = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_LANDLORD];
        $this->contactPersonPhone = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON_PHONE];
        $this->room = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_ROOM];
        $this->last_booking = $bookingObjectData[BookingObjectEnum::BOOKING_OBJECT_LAST_BOOKING_DATE_FROM]['date']??null;

        return $this;
    }

    public function getObjectName(): ?string
    {
        return $this->objectName;
    }

    public function setObjectName($objectName): self
    {
        $this->objectName = $objectName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress($address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getContactPerson(): ?string
    {
        return $this->contactPerson;
    }

    public function setContactPerson($contactPerson): self
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    public function getContactPersonPhone(): ?string
    {
        return $this->contactPersonPhone;
    }

    public function setContactPersonPhone($contactPersonPhone): self
    {
        $this->contactPersonPhone = $contactPersonPhone;

        return $this;
    }

    public function getPricePerNight()
    {
        return $this->pricePerNight;
    }

    public function setPricePerNight($pricePerNight): self
    {
        $this->pricePerNight = $pricePerNight;

        return $this;
    }

    /**
     * @param $numberOfRooms
     * @return BookingObject
     */
    public function setNumberOfRooms($numberOfRooms): BookingObject
    {
        $this->numberOfRooms = intval($numberOfRooms);
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberOfRooms(): ?int
    {
        return $this->numberOfRooms;
    }

    /**
     * @param mixed $object_type
     * @return BookingObject
     */
    public function setObjectType($object_type)
    {
        $this->object_type = $object_type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     * @param $landlordEmail
     * @return BookingObject
     */
    public function setLandlordEmail($landlordEmail)
    {
        $this->landlordEmail = $landlordEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getLandlordEmail()
    {
        return $this->landlordEmail;
    }

    /**
     * @param mixed $room
     * @return BookingObject
     */
    public function setRoom($room)
    {
        $this->room = $room;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param mixed $last_booking
     * @return BookingObject
     */
    public function setLastBooking($last_booking)
    {
        $this->last_booking = $last_booking;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastBooking()
    {
        return $this->last_booking;
    }
}
