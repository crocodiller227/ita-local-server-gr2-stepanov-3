<?php

namespace App\Entity;

use App\Model\Enum\BookingObjectEnum;
use Doctrine\ORM\Mapping as ORM;

class Cottage extends BookingObject
{

    /**
     * @var boolean
     */
    private $kitten;

    /**
     * @var boolean
     */
    private $bathroom;

    /**
     * @var boolean
     */
    private $swimmingPool;

    /**
     * @var boolean
     */
    private $billiards;

    /**
     * @var boolean
     */
    private $sauna;

    public function __construct()
    {
        $this->object_type = BookingObjectEnum::BOOKING_OBJECT_COTTAGE;
    }

    /**
     * @return array
     */
    public function __toArray()
    {
        return array_merge(parent::__toArray(), [
            BookingObjectEnum::BOOKING_COTTAGE_SAUNA => $this->sauna,
            BookingObjectEnum::BOOKING_COTTAGE_KITTEN => $this->kitten,
            BookingObjectEnum::BOOKING_COTTAGE_BATHROOM => $this->bathroom,
            BookingObjectEnum::BOOKING_COTTAGE_BILLIARDS => $this->billiards,
            BookingObjectEnum::BOOKING_COTTAGE_SWIMMING_POOL => $this->swimmingPool,
        ]);
    }

    /**
     * @param array $bookingObjectData
     * @return $this
     */
    public function __fromArray(array $bookingObjectData)
    {
        parent::__fromArray($bookingObjectData);
        $this->sauna = $bookingObjectData[BookingObjectEnum::BOOKING_COTTAGE_SAUNA] ?? false;
        $this->kitten = $bookingObjectData[BookingObjectEnum::BOOKING_COTTAGE_KITTEN] ?? false;
        $this->bathroom = $bookingObjectData[BookingObjectEnum::BOOKING_COTTAGE_BATHROOM] ?? false;
        $this->billiards = $bookingObjectData[BookingObjectEnum::BOOKING_COTTAGE_BILLIARDS] ?? false;
        $this->swimmingPool = $bookingObjectData[BookingObjectEnum::BOOKING_COTTAGE_SWIMMING_POOL] ?? false;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param bool $bathroom
     * @return Cottage
     */
    public function setBathroom(bool $bathroom): Cottage
    {
        $this->bathroom = $bathroom;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBathroom(): bool
    {
        return $this->bathroom;
    }

    /**
     * @param bool $kitten
     * @return Cottage
     */
    public function setKitten(bool $kitten): Cottage
    {
        $this->kitten = $kitten;
        return $this;
    }

    /**
     * @return bool
     */
    public function isKitten(): bool
    {
        return $this->kitten;
    }

    /**
     * @param bool $swimmingPool
     * @return Cottage
     */
    public function setSwimmingPool(bool $swimmingPool): Cottage
    {
        $this->swimmingPool = $swimmingPool;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSwimmingPool(): bool
    {
        return $this->swimmingPool;
    }

    /**
     * @param bool $billiards
     * @return Cottage
     */
    public function setBilliards(bool $billiards): Cottage
    {
        $this->billiards = $billiards;
        return $this;
    }

    /**
     * @return bool
     */
    public function isBilliards(): bool
    {
        return $this->billiards;
    }

    /**
     * @param bool $sauna
     * @return Cottage
     */
    public function setSauna(bool $sauna): Cottage
    {
        $this->sauna = $sauna;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSauna(): bool
    {
        return $this->sauna;
    }

}
