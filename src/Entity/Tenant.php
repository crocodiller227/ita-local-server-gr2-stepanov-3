<?php

namespace App\Entity;

use App\Model\Enum\UserEnum;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TenantRepository")
 */
class Tenant extends User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function __construct()
    {
        parent::__construct();
        $this->addRoles(UserEnum::ROLE_TENANT);
    }


    public function getId()
    {
        return $this->id;
    }

}
