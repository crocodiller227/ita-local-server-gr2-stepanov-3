<?php

namespace App\Entity;

use App\Model\Enum\UserEnum;
use App\Model\Enum\SocialEnum;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Entity
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"User" = "User", "Tenant" = "Tenant", "Landlord" = "Landlord"})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("email")
 * @UniqueEntity("passportId")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $full_name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)idid
     */
    protected $password;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $enabled;


    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $roles;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $passportId;

    /**
     * The salt to use for hashing.
     *
     * @var string
     */
    protected $salt;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    protected $socialFacebook;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    protected $socialVkontakte;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    protected $socialGoogle;


    public function __construct()
    {
        $this->enabled = false;
        $this->roles = json_encode([UserEnum::ROLE_USER]);
    }

    /**
     * @return array
     */
    public function __toArray()
    {
        return [
            UserEnum::EMAIL => $this->email,
            UserEnum::ENABLED => $this->enabled,
            UserEnum::PASSWORD => $this->password,
            UserEnum::USERNAME => $this->username,
            UserEnum::FULL_NAME => $this->full_name,
            UserEnum::PASSPORT_ID => $this->passportId,
            UserEnum::GOOGLE => $this->socialGoogle,
            UserEnum::FACEBOOK => $this->socialFacebook,
            UserEnum::VKONTAKTE => $this->socialVkontakte,
            UserEnum::ROLES => json_decode($this->roles, true),
        ];
    }

    /**
     * @param array $userData
     * @return $this
     */
    public function __fromArray(array $userData)
    {
        $this->email = $userData[UserEnum::EMAIL];
        $this->username = $userData[UserEnum::USERNAME];
        $this->password = $userData[UserEnum::PASSWORD];
        $this->passportId = $userData[UserEnum::PASSPORT_ID];
        $this->enabled = $userData[UserEnum::ENABLED] ?? false;
        $this->full_name = $userData[UserEnum::FULL_NAME] ?? null;
        $this->socialGoogle = $userData[UserEnum::GOOGLE] ?? null;
        $this->socialFacebook = $userData[UserEnum::FACEBOOK] ?? null;
        $this->socialVkontakte = $userData[UserEnum::VKONTAKTE] ?? null;

        return $this;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }


    /**
     * @return null | string
     */
    public function getFullName(): ?string
    {
        return $this->full_name;
    }

    /**
     * @param string | null $full_name
     * @return User
     */
    public function setFullName(?string $full_name): self
    {
        $this->full_name = $full_name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string | null $email
     * @return User
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return User
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }


    /**
     * @return array The user roles
     */
    public function getRoles(): array
    {
        return json_decode($this->roles, true);
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = json_encode($roles);
        return $this;
    }

    /**
     * @param string $roles
     * @return User
     */
    public function addRoles($roles)
    {
        $decodedRoles = json_decode($roles, true);
        if (gettype($decodedRoles) == 'array') {
            $this->roles = json_encode(
                array_values(
                    array_filter(
                        array_unique(
                            array_merge(
                                json_decode($this->roles, true) ?? [],
                                $decodedRoles
                            )
                        )
                    )
                )
            );
        } else {
            $current_roles = json_decode($this->roles, true) ?? [];
            $current_roles[] = $roles;
            $this->roles = json_encode($current_roles);
        }
        return $this;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        $this->password = null;
    }

    /**
     * @param string $passportId
     * @return User
     */
    public function setPassportId(string $passportId): User
    {
        $this->passportId = $passportId;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassportId(): ?string
    {
        return $this->passportId;
    }

    /**
     * @param string | null $socialFacebook
     * @return User
     */
    public function setSocialFacebook(?string $socialFacebook): User
    {
        $this->socialFacebook = $socialFacebook;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocialFacebook(): string
    {
        return $this->socialFacebook;
    }

    /**
     * @param string | null $socialVkontakte
     * @return User
     */
    public function setSocialVkontakte(?string $socialVkontakte): User
    {
        $this->socialVkontakte = $socialVkontakte;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocialVkontakte(): string
    {
        return $this->socialVkontakte;
    }

    /**
     * @param string | null $socialGoogle
     * @return User
     */
    public function setSocialGoogle(?string $socialGoogle): User
    {
        $this->socialGoogle = $socialGoogle;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocialGoogle(): string
    {
        return $this->socialGoogle;
    }

    /**
     * @param array $socialData
     * @return User | boolean
     */
    public function bindSocialNetwork(array $socialData): self
    {
        switch ($socialData[SocialEnum::NETWORK]) {
            case SocialEnum::FACEBOOK:
                $this->socialFacebook = $socialData[SocialEnum::UID];
                break;
            case SocialEnum::VKONTAKTE :
                $this->socialVkontakte = $socialData[SocialEnum::UID];
                break;
            case SocialEnum::GOOGLE:
                $this->socialGoogle = $socialData[SocialEnum::UID];
                break;
            default:
                return false;
        }
        return $this;
    }

}
