<?php


namespace App\Form;


use App\Model\Enum\BookingObjectEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegisterBookingObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(BookingObjectEnum::BOOKING_OBJECT_TYPE, ChoiceType::class, [
                'choices' => [
                    'Cottage' => BookingObjectEnum::BOOKING_OBJECT_COTTAGE,
                    'Pension' => BookingObjectEnum::BOOKING_OBJECT_PENSION
                ]
            ])
            ->add(BookingObjectEnum::BOOKING_OBJECT_NAME)
            ->add(BookingObjectEnum::BOOKING_OBJECT_NUMBER_OF_ROOMS, NumberType::class)
            ->add(BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON)
            ->add(BookingObjectEnum::BOOKING_OBJECT_CONTACT_PERSON_PHONE)
            ->add(BookingObjectEnum::BOOKING_OBJECT_ADDRESS, TextType::class, [
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add(BookingObjectEnum::BOOKING_OBJECT_PRICE_PER_NIGHT, NumberType::class)
            ->add('next_step_>', SubmitType::class);
    }
}