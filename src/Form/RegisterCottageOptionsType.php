<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class RegisterCottageOptionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kitten', CheckboxType::class, [
                'required' => false,
            ])
            ->add('bathroom', CheckboxType::class, [
                'required' => false,
            ])
            ->add('swimming_pool', CheckboxType::class, [
                'required' => false,
            ])
            ->add('billiards', CheckboxType::class, [
                'required' => false,
            ])
            ->add('sauna', CheckboxType::class, [
                'required' => false,
            ])
            ->add('register', SubmitType::class);
    }
}