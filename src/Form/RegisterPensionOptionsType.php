<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class RegisterPensionOptionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('television_set', CheckboxType::class, [
                'required' => false,
            ])
            ->add('shower_in_the_room', CheckboxType::class, [
                'required' => false,
            ])
            ->add('air_conditioning', CheckboxType::class, [
                'required' => false,
            ])
            ->add('cleaning_of_rooms', CheckboxType::class, [
                'required' => false,
            ])
            ->add('three_meals_a_day', CheckboxType::class, [
                'required' => false,
            ])
            ->add('register', SubmitType::class);
    }
}