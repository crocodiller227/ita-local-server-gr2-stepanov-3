<?php


namespace App\Form;


use App\Model\Enum\UserEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(UserEnum::USERNAME, TextType::class)
            ->add(UserEnum::EMAIL, EmailType::class)
            ->add(UserEnum::PASSPORT_ID, TextType::class)
            ->add(UserEnum::PASSWORD, PasswordType::class)
            ->add(UserEnum::ROLES, ChoiceType::class, [
                'choices' =>
                    [
                        'Landlord' => 'landlord',
                        'Tenant' => 'tenant'
                    ],
            ])
            ->add('register', SubmitType::class);

        $user = $builder->getData();

        $builder->get(UserEnum::ROLES)->addModelTransformer(new CallbackTransformer(
            function () use (&$user) {
                $currentUserRoles = $user->getRoles();
                if (in_array(UserEnum::ROLE_TENANT, $currentUserRoles)) {
                    return 'tenant';
                } elseif (in_array(UserEnum::ROLE_LANDLORD, $currentUserRoles)) {
                    return 'landlord';
                } else {
                    return null;
                }
            },
            function ($roles) use (&$user) {
                $currentRoles = array_diff($user->getRoles(), [UserEnum::ROLE_TENANT, UserEnum::ROLE_LANDLORD]);
                switch ($roles) {
                    case 'tenant':
                        $currentRoles[] = UserEnum::ROLE_TENANT;
                        break;
                    case 'landlord':
                        $currentRoles[] = UserEnum::ROLE_LANDLORD;
                        break;
                    default:
                        $user->getRoles();
                        break;
                }
                return $currentRoles;
            }
        ));
    }
}