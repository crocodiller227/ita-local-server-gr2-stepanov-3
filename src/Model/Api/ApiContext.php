<?php

namespace App\Model\Api;


use App\Model\Enum\UserEnum;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT_CREATE = '/client/create';
    const ENDPOINT_EXIST_CLIENT = '/client/#passport_id#/#email#';
    const ENDPOINT_CHECK_CLIENT = '/client/check/#email#/#password#';
    const ENDPOINT_GET_CLIENT = '/client/get';
    const ENDPOINT_CLIENT_CHANGE_PASSWORD = '/client/change_password';
    const ENDPOINT_CLIENT_BIND_SOCIAL = '/client/bind_social';
    const ENDPOINT_CHECK_CLIENT_BY_SOCIAL = '/client/check_by_social/#network#/#uid#';
    const ENDPOINT_EDIT_CLIENT = '/client/edit';
    const ENDPOINT_HASHING_PASSWORD = '/client/password/hashing';
    const ENDPOINT_CREATE_OBJECT = '/object/create';
    const ENDPOINT_GET_ALL_LANDLORD_OBJECTS = '/client/landlord/object/get_all/#email#';
    const ENDPOINT_OBJECT_GET_ALL = '/object/get/all';
    const ENDPOINT_OBJECT_GET_FILTER = '/object/get/filter';
    const ENDPOINT_OBJECT_GET_LAST_BOOKING_DATE = '/object/booking/last';
    const ENDPOINT_BOOKING_CREATE = '/object/booking/create';
    const CHECK_TENANT_THERE_ANY_BOOKING_AT_THIS_TIME = '/client/booking/check/#email#';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_EXIST_CLIENT, [
            'passport_id' => $passport,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT_CREATE, self::METHOD_POST, $data);
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     * @throws ApiException
     */
    public function clientCheck($email, $password)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT, [
            'email' => $email,
            'password' => $password
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function getClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_GET_CLIENT, self::METHOD_GET, $data);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function changePassword($data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT_CHANGE_PASSWORD, self::METHOD_PUT, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function bindSocialNetwork($data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT_BIND_SOCIAL, self::METHOD_PUT, $data);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function checkClientBySocial($data)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_BY_SOCIAL, [
            'network' => $data['network'],
            'uid' => $data['uid']
        ]);
        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function editUser(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_EDIT_CLIENT, self::METHOD_PUT, $data);
    }

    /**
     * @param $plainPassword
     * @return mixed
     * @throws ApiException
     */
    public function getHashingPassword($plainPassword)
    {
        return $this->makeQuery(self::ENDPOINT_HASHING_PASSWORD,
            self::METHOD_GET,
            ['plainPassword' => $plainPassword]
        )['result'];
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createNewObject(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CREATE_OBJECT, self::METHOD_POST, $data);
    }

    /**
     * @param string $email
     * @return mixed
     * @throws ApiException
     */
    public function getLandlordObjects(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_GET_ALL_LANDLORD_OBJECTS, [
            'email' => $email
        ]);
        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function getAllBookingObjects()
    {
        return $this->makeQuery(self::ENDPOINT_OBJECT_GET_ALL, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function getBookingObjectsWithFilter(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_OBJECT_GET_FILTER, self::METHOD_GET, $data);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function createBooking($data)
    {
        return $this->makeQuery(self::ENDPOINT_BOOKING_CREATE, self::METHOD_POST, $data);
    }


}