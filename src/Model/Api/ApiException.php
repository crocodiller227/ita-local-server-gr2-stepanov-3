<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 15.06.18
 * Time: 19:54
 */

namespace App\Model\Api;


use Throwable;

class ApiException extends \Exception
{
    /**
     * @var mixed
     */
    private $response;

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null, $response = null)
    {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
