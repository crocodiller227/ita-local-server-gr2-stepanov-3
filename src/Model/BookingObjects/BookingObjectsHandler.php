<?php


namespace App\Model\BookingObjects;


use App\Entity\Cottage;
use App\Entity\Pension;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\Enum\BookingObjectEnum;

class BookingObjectsHandler
{

    public function arrayIntoArrayEntities(
        array $arrayWithToArrayEntities
    )
    {
        $bookingObjects = [];
        foreach ($arrayWithToArrayEntities as $object) {
            if ($object[BookingObjectEnum::BOOKING_OBJECT_TYPE] == BookingObjectEnum::BOOKING_OBJECT_COTTAGE) {
                $objectToArray = new Cottage();
                $objectToArray->__fromArray($object);
            } elseif ($object[BookingObjectEnum::BOOKING_OBJECT_TYPE] == BookingObjectEnum::BOOKING_OBJECT_PENSION) {
                $objectToArray = new Pension();
                $objectToArray->__fromArray($object);
            }

            array_push($bookingObjects, $objectToArray);
        }
        return $bookingObjects;
    }
}