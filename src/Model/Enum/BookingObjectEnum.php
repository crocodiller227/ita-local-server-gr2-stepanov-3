<?php

namespace App\Model\Enum;

class BookingObjectEnum
{
    const BOOKING_OBJECT_TYPE = 'object_type';
    const BOOKING_OBJECT_PENSION = 'pension';
    const BOOKING_OBJECT_COTTAGE = 'cottage';
    const BOOKING_OBJECT_NAME = 'object_name';
    const BOOKING_OBJECT_ADDRESS = 'address';
    const BOOKING_OBJECT_CONTACT_PERSON = 'contact_person';
    const BOOKING_OBJECT_CONTACT_PERSON_PHONE = 'contact_person_phone';
    const BOOKING_OBJECT_NUMBER_OF_ROOMS= 'number_of_rooms';
    const BOOKING_OBJECT_PRICE_PER_NIGHT = 'price_per_night';
    const BOOKING_OBJECT_LANDLORD = 'landlord';
    const BOOKING_OBJECT_LAST_BOOKING = 'last_booking';
    const BOOKING_OBJECT_ROOM = 'room';
    const BOOKING_OBJECT_LAST_BOOKING_DATE_FROM = 'last_booking_date_from';


    const BOOKING_PENSION_TELEVISION_SET = 'television_set';
    const BOOKING_PENSION_SHOWER_IN_THE_ROOM = 'shower_in_the_room';
    const BOOKING_PENSION_AIR_CONDITIONING = 'air_conditioning';
    const BOOKING_PENSION_CLEANING_OF_ROOMS = 'cleaning_of_rooms';
    const BOOKING_PENSION_THREE_MEALS_A_DAY = 'three_meals_a_day';

    const BOOKING_COTTAGE_KITTEN = 'kitten';
    const BOOKING_COTTAGE_BATHROOM = 'bathroom';
    const BOOKING_COTTAGE_SWIMMING_POOL = 'swimming_pool';
    const BOOKING_COTTAGE_BILLIARDS = 'billiards';
    const BOOKING_COTTAGE_SAUNA = 'sauna';
}