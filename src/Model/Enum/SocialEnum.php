<?php


namespace App\Model\Enum;


class SocialEnum
{
    const NETWORK = 'network';
    const UID = 'uid';
    const FACEBOOK = 'facebook';
    const VKONTAKTE = 'vkontakte';
    const GOOGLE = 'google';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const ULOGIN_DATA = 'ulogin-data';

}