<?php

namespace App\Model\Enum;

class UserEnum
{
    const ID = 'id';
    const USERNAME = 'username';
    const FULL_NAME = 'full_name';
    const PASSWORD = 'password';
    const EMAIL = 'email';
    const PASSPORT_ID = 'passport_id';
    const FACEBOOK = 'socialFacebook';
    const VKONTAKTE = 'socialVkontakte';
    const GOOGLE = 'socialGoogle';
    const ROLES = 'roles';
    const ENABLED = 'enabled';
    const ROLE_TENANT = 'ROLE_TENANT';
    const ROLE_LANDLORD = 'ROLE_LANDLORD';
    const ROLE_USER= 'ROLE_USER';
}