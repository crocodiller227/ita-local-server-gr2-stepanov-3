<?php


namespace App\Model\User;

use App\Entity\Landlord;
use App\Entity\Tenant;
use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserHandler
{

    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var ApiContext
     */
    private $apiContext;

    public function __construct(
        ContainerInterface $container, ApiContext $apiContext
    )
    {
        $this->container = $container;
        $this->apiContext = $apiContext;
    }

    /**
     * @param User $user
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws ApiException
     */
    public function createAbstractUser($user, array $data, $encodePassword = true)
    {
        $user->__fromArray($data);

        if ($encodePassword) {
            $password = $this->passwordHashing($data['password']);
        } else {
            $password = $data['password'];
        }

        $user->setPassword($password);

        return $user;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws ApiException
     */
    public function createNewUser(array $data, $encodePassword = true)
    {
        return $this->createAbstractUser(new User(), $data, $encodePassword);
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws ApiException
     */
    public function createNewLandlord(array $data, $encodePassword = true)
    {
        return $this->createAbstractUser(new Landlord(), $data, $encodePassword);
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return User
     * @throws ApiException
     */
    public function createNewTenant(array $data, $encodePassword = true)
    {
        return $this->createAbstractUser(new Tenant(), $data, $encodePassword);
    }

    /**
     * @param string $plainPassword
     * @return string
     * @throws ApiException
     */
    public function passwordHashing(string $plainPassword)
    {
        return $this->apiContext->getHashingPassword($plainPassword);
    }

    /**
     * @param User $user
     */
    public function insertUserInSession(User $user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
    }


}