<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findUserByEmailAndPassword($email, $password)
    {
        try {
            return $this
                ->createQueryBuilder('u')
                ->select('u')
                ->where('u.email = :email')
                ->andWhere('u.password = :password')
                ->setParameter('password', $password)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param $email
     * @return User | null
     */
    public function findUserByEmail($email)
    {
        try {
            return $this
                ->createQueryBuilder('u')
                ->select('u')
                ->where('u.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param $socialData
     * @return User | null
     * @throws NonUniqueResultException
     */
    public function findUserBySocial($socialData)
    {
        switch ($socialData['network']) {
            case 'facebook':
                $user = $this
                    ->createQueryBuilder('u')
                    ->select('u')
                    ->where('u.socialFacebook = :uid')
                    ->setParameter('uid', $socialData['uid'])
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                break;
            case 'vkontakte':
                $user = $this
                    ->createQueryBuilder('u')
                    ->select('u')
                    ->where('u.socialVkontakte = :uid')
                    ->setParameter('uid', $socialData['uid'])
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                break;
            case 'google':
                $user = $this
                    ->createQueryBuilder('u')
                    ->select('u')
                    ->where('u.socialGoogle = :uid')
                    ->setParameter('uid', $socialData['uid'])
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
                break;
            default:
                return null;
        }
        return $user;
    }
}
